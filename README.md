# shogun-wazops

- docker-compose orchestrator

- Get docker
```bash
git clone https://github.com/nu11secur1ty/shogun-wazops.git
```
- Compose
```
docker-compose up -d
```

- Open phpmyadmin at [http://localhost:8000](http://localhost:8000)

=====================================================================

- Open web browser to look php7 at [http://localhost:8001/shogun-ops.html](http://localhost:8001/shogun-ops.html)

=====================================================================

- Open app.php on [http://localhost:8001/shogun-ops.html](http://localhost:8001/shogun-ops.html)

Enjoy !
